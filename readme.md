## Front-End Home page

![Home page](https://www.dropbox.com/s/eo79umo8noef8a8/home.jpg?dl=1)

## Front-End Estates page

![Estates Page](https://www.dropbox.com/s/u5yi3befsap8c5y/estate.jpg?dl=1)

## Front-End Estates Show page

![Estates Show Page](https://www.dropbox.com/s/krzsh4083n6f4th/estate-show.png?dl=1)

## Front-End Responsive

![Mobile](https://www.dropbox.com/s/cbl4r39wxlizzp6/mobile.png?dl=1)
