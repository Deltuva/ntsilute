<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'pages.home')->name('page.home');
Route::view('/parduodamas-nt-siluteje', 'estate.listing')->name('estate.listing');
Route::view('/content/show', 'content.show')->name('content.show');
Route::view('/apie-mus', 'pages.about')->name('page.about');
Route::view('/paslaugos', 'pages.service')->name('page.service');
Route::view('/kontaktai', 'pages.contact')->name('page.contact');
