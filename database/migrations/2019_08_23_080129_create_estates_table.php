<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('single_image')->nullable();
            $table->text('multi_images')->nullable();
            $table->string('name')->unique();
            $table->integer('category_id')->unsigned();
            $table->string('address')->nullable();
            $table->string('area_of_apartment')->nullable();
            $table->string('number_of_apartment')->nullable();
            $table->string('floor_of_apartment')->nullable();
            $table->string('area_of_premise')->nullable();
            $table->string('window_of_apartment')->nullable();
            $table->string('area_of_house')->nullable();
            $table->string('heating')->nullable();
            $table->string('plotarea_of_house')->nullable();
            $table->string('electra')->nullable();
            $table->string('water_supply')->nullable();
            $table->string('cellar')->nullable();
            $table->float('price');
            $table->string('status')->nullable();
            $table->longText('body');
            $table->string('slug')->nullable();
            $table->string('year')->nullable();
            $table->timestamps();
        });

        Schema::table('estates', function($table) {
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estates');
    }
}
