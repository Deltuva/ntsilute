@extends('layouts.app')

@section('title', 'PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE - NT skelbimas Šilutėje')
@section('content')

@include('include.headerNoSlider')

<section class="sect-content section-content">

    <div class="container">

        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-12 order-first order-lg-2 order-md-2 mb-4">
                <div class="photopic">
                    <div class="photopic-singlephoto">
                        <a href="{{ asset('images/estates/single.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip">
                            <img class="img-fluid" src="{{ asset('images/estates/1.jpg') }}"
                                alt="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE">
                            <span class="status-parduota"></span>
                        </a>
                    </div>

                    <div class="photopic-multiphotos">
                        <a href="{{ asset('images/estates/sm/1.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/1.jpg') }}" alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE"
                                class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/2.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/2.jpg') }}" alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE"
                                class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/3.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/3.jpg') }}" alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE"
                                class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/4.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/4.jpg') }}" alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE"
                                class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/5.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/5.jpg') }}" alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE"
                                class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/6.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/6.jpg') }}" alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE"
                                class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/7.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/7.jpg') }}" alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE"
                                class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/8.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/8.jpg') }}" alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE"
                                class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/9.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/9.jpg') }}" alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE"
                                class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/10.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/10.jpg') }}"
                                alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE" class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/11.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/11.jpg') }}"
                                alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE" class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/12.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/12.jpg') }}"
                                alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE" class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/13.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/13.jpg') }}"
                                alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE" class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/14.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/14.jpg') }}"
                                alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE" class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/15.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/15.jpg') }}"
                                alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE" class="photo img-fluid"></a>
                        <a href="{{ asset('images/estates/sm/16.jpg') }}"
                            title="PARDUODAMAS  2  KAMBARIŲ  BUTAS  3 AUKŠTE" data-lightbox="roadtrip"><img
                                src="{{ asset('images/estates/sm/16.jpg') }}"
                                alt="PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE" class="photo img-fluid"></a>
                    </div>
                    <!-- /.photopic-multiphotos -->

                </div>
                <!-- /.photopic -->
            </div>
            <!-- /.col-lg-6 -->

            <div class="col-lg-6 col-md-6 col-sm-12">
                <h1>PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE</h1>
                <h2>26000&euro;</h2>

                <div class="desc">
                    <div class="desc-line">
                        <span class="desc-line--l">Adresas</span>
                        <span class="desc-line--r">Žalgirio g. 7-10, Šilutė</span>
                    </div>
                    <div class="desc-line">
                        <span class="desc-line--l">Plotas</span>
                        <span class="desc-line--r">36.57</span>
                    </div>
                    <div class="desc-line">
                        <span class="desc-line--l">Kambarių sk.</span>
                        <span class="desc-line--r">2</span>
                    </div>
                    <div class="desc-line">
                        <span class="desc-line--l">Aukštas</span>
                        <span class="desc-line--r">3</span>
                    </div>
                    <div class="desc-line">
                        <span class="desc-line--l">Rūsys</span>
                        <span class="desc-line--r">Nėra</span>
                    </div>
                    <div class="desc-line">
                        <span class="desc-line--l">Statybos metai</span>
                        <span class="desc-line--r">1961</span>
                    </div>
                </div>
                <!-- /.desc -->

                <div class="text">
                    <p>PARDUODAMAS JAUKUS,&nbsp;&nbsp;ŠVIESUS IR ERDVUS DVIEJŲ KAMBARIŲ BUTAS TREČIAME AUKŠTE. VIRTUVĖ
                        SUJUNGTA SU SVETAINE, MIEGAMASIS KAMBARYS TURI NIŠĄ - DRABUŽINĘ, VONIA IR WC KARTU, PAKEISTA
                        SANTECHNIKA, NAUJA ELEKTROS INSTALIACIJA.&nbsp;SVETAINĘ PUOŠIA AUTENTIŠKA RANKŲ
                        DARBO&nbsp;KOKLINĖ KROSNIS KURI APŠILDO NAMUS IR YRA UNIKALI TUOM, KAD ŠILDANT ORAS PATALPOSE
                        NEIŠSAUSĖJA IR ILGIAU IŠSILAIKO ŠILUMA ( PAPILDOMAI &nbsp;GALIMA ĮSIVESTI ŠILDYMĄ ORAS - ORAS) .
                        TVARKINGA IR PRIŽIŪRĖTA NAMO APLINKA, DRAUGIŠKI KAIMYNAI.</p>
                    <p>BENDRA INFORMACIJA:</p>
                    <p>Adresas: Šilutė, Žalgirio g. 7-10</p>
                    <p>Statybos pradžios metai: 1961 m.</p>
                    <p>Kambarių skaičius: 2</p>
                    <p>Aukštas: 3</p>
                    <p>Rūsys: nėra</p>
                    <p>Šildymas: krosninis šildymas</p>
                    <p>Vandentiekis: komunalinis vandentiekis</p>
                    <p>Nuotekų šalinimas: komunalinis nuotekų šalinimas</p>
                    <p>Bendras plotas: 36.57 kv.m.,</p>
                    <p>Namą administruoja: Šilutės būstas</p>
                    <p>IŠPLANAVIMAS:<br>
                        Koridorius: 4.84 kv.m.,</p>
                    <p>San. mazgas: 2.46 kv.m.,</p>
                </div>
                <!-- /.text -->

                <div class="contact">
                    <div class="contact-call__title">
                        <h3>Susidomėjus prašome kreiptis</h3>
                        <h4>Telefonu:</h4>
                    </div>
                    <!-- /.contact-call__title -->

                    <div class="contact-call__phones">
                        <a class="number" href="tel:0037061120364"><i class="fas fa-phone"></i> +370 611 20364</a>
                        <a class="number" href="tel:0037068651779"><i class="fas fa-phone"></i> +370 686 51779</a>
                    </div>
                    <!-- /.contact-call__phones -->

                    <div class="contact-call__send">
                        <span>arba</span>

                        <div class="send-btn" data-toggle="modal" data-target="#modal">
                            <i class="far fa-envelope"></i> Siųsti užklausą
                        </div>
                    </div>
                    <!-- /.contact-call__send -->

                </div>
                <!-- /.contact -->

                <div class="go-back">
                    <a href="javascript:history.back()">‹ Atgal
                    </a>
                </div>
            </div>
            <!-- /.col-lg-6 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

</section>
<!-- /.estates section -->

@endsection