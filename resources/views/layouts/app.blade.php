<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="application-name" content="NT Šilutė" />
    <meta name="msapplication-TileColor" content="#59c355" />

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.ico') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/76x76.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/120x120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/152x152.png') }}">

    <!-- Styles -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/css/lightbox.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        @yield('content')
        @include('include.footer')
        @include('include.modal')
    </div>

    <!-- Scripts -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/js/lightbox-plus-jquery.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    @stack('scripts')

</body>

</html>
