@extends('layouts.app')

@section('title', 'Nekilnojamojo turto agentūra Šilutėje - NT Šilutė')
@section('content')

@include('include.header')

<section class="sect-estates section">

    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="section-title">Nekilnojamojo turto skelbimai</h1>
            </div>
        </div>

        <div class="categories">
            <ul class="categories-list">
                <li class="categories-list--item"><a class="icn-butai" href="#">Butai</a></li>
                <li class="categories-list--item"><a class="icn-komercines-patalpos" href="#">Komercinės patalpos</a>
                </li>
                <li class="categories-list--item"><a class="icn-namai" href="#">Namai</a></li>
                <li class="categories-list--item"><a class="icn-sklypai" href="#">Sklypai</a></li>
                <li class="categories-list--item"><a class="icn-sodybos" href="#">Sodybos</a></li>
            </ul>
        </div>
        <!-- /.categories -->

    </div>

</section>
<!-- /.estates section -->

<section class="sect-adverts section">

    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="section-title">Naujausi skelbimai</h1>
            </div>
        </div>

        <div class="estate">
            <div class="row">

                <div class="estate-block col-lg-4 col-sm-6 col-md-6 col-12 ml-auto mr-auto">
                    <div class="estate-item">
                        <div class="estate-item--img">
                            <a href="{{ route('content.show') }}"><img class="img-fluid" src="{{ asset('images/estates/1.jpg') }}"
                                    alt="Image"></a>
                        </div>
                        <div class="estate-item--content">
                            <a href="{{ route('content.show') }}">PARDUODAMAS 2 KAMBARIŲ BUTAS 2 AUKŠTE</a>

                            <div class="estate-item--content-line">
                                <span class="estate-item--content-line--l">Adresas</span>
                                <span class="estate-item--content-line--r">Žalgirio g. 7-10, Šilutė</span>
                            </div>
                            <div class="estate-item--content-line">
                                <span class="estate-item--content-line--l">Buto plotas</span>
                                <span class="estate-item--content-line--r">36.57</span>
                            </div>
                            <div class="estate-item--content-line">
                                <span class="estate-item--content-line--l">Kambarių sk.</span>
                                <span class="estate-item--content-line--r">2</span>
                            </div>
                            <div class="estate-item--content-line">
                                <span class="estate-item--content-line--l">Aukštas</span>
                                <span class="estate-item--content-line--r">2</span>
                            </div>

                        </div>
                        <!-- /.estate-item--content -->

                        <div class="estate-item--price">
                            <div class="price">26000&euro;</div>
                            <div><a class="item-btn" href="{{ route('content.show') }}">Žiūrėti
                                    <i class="fas fa-chevron-right float-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <!-- /.estate-block -->

                <div class="estate-block col-lg-4 col-sm-6 col-md-6 col-12 ml-auto mr-auto">
                    <div class="estate-item">
                        <div class="estate-item--img">
                            <a href="{{ route('content.show') }}"><img class="img-fluid" src="{{ asset('images/estates/2.jpg') }}"
                                    alt="Image"></a>
                            <span class="status-rezervuota"></span>
                        </div>
                        <div class="estate-item--content">
                            <a href="{{ route('content.show') }}">PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE</a>

                            <div class="estate-item--content-line">
                                <span class="estate-item--content-line--l">Adresas</span>
                                <span class="estate-item--content-line--r">Žalgirio g. 7-10, Šilutė</span>
                            </div>
                            <div class="estate-item--content-line">
                                <span class="estate-item--content-line--l">Buto plotas</span>
                                <span class="estate-item--content-line--r">36.57</span>
                            </div>
                            <div class="estate-item--content-line">
                                <span class="estate-item--content-line--l">Kambarių sk.</span>
                                <span class="estate-item--content-line--r">2</span>
                            </div>
                            <div class="estate-item--content-line">
                                <span class="estate-item--content-line--l">Aukštas</span>
                                <span class="estate-item--content-line--r">3</span>
                            </div>

                        </div>
                        <!-- /.estate-item--content -->

                        <div class="estate-item--price">
                            <div class="price">36000&euro;</div>
                            <div><a class="item-btn" href="{{ route('content.show') }}">Žiūrėti <i
                                        class="fas fa-chevron-right float-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <!-- /.estate-block -->

                <div class="estate-block col-lg-4 col-sm-6 col-md-6 col-12 ml-auto mr-auto">
                    <div class="estate-item">
                        <div class="estate-item--img">
                            <a href="{{ route('content.show') }}"><img class="img-fluid" src="{{ asset('images/estates/3.jpg') }}"
                                    alt="Image"></a>
                            <span class="status-parduota"></span>
                        </div>
                        <div class="estate-item--content">
                            <a href="{{ route('content.show') }}">PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE</a>

                            <div class="estate-item--content-line">
                                <span class="estate-item--content-line--l">Adresas</span>
                                <span class="estate-item--content-line--r">Žalgirio g. 7-10, Šilutė</span>
                            </div>
                            <div class="estate-item--content-line">
                                <span class="estate-item--content-line--l">Buto plotas</span>
                                <span class="estate-item--content-line--r">36.57</span>
                            </div>
                            <div class="estate-item--content-line">
                                <span class="estate-item--content-line--l">Kambarių sk.</span>
                                <span class="estate-item--content-line--r">2</span>
                            </div>
                            <div class="estate-item--content-line">
                                <span class="estate-item--content-line--l">Aukštas</span>
                                <span class="estate-item--content-line--r">3</span>
                            </div>

                        </div>
                        <!-- /.estate-item--content -->

                        <div class="estate-item--price">
                            <div class="price">6000&euro;</div>
                            <div><a class="item-btn" href="{{ route('content.show') }}">Žiūrėti
                                    <i class="fas fa-chevron-right float-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <!-- /.estate-block -->

            </div>

            <div class="estate-bottom">
                <a class="bottom-btn" href="#">Žiūrėti daugiau
                    <i class="fas fa-chevron-right"></i></a>
            </div>
            <!-- /.estate-bottom -->

        </div>
        <!-- /.section-container -->

    </div>

</section>
<!-- /.sect-adverts section -->

<section class="sect-about">

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-6 no-padding">
                <div class="info">
                    <h1 class="section-title">Apie mus</h1>
                    <p>NT Šilutė - Nekilnojamojo turto agentūra Šilutėje įkurta 2008m. ir nuo to laiko sėkmingai
                        dirbanti nekilnojamojo turto pirkimo, pardavimo bei nuomos srityse. Esame viena iš pirmųjų
                        Šilutės miesto agentūrų, kuri savo pastoviu ir kvalifikuotu darbu, bei lanksčia mokesčių sistema
                        pelnėme klientų pasitikėjimą. Mes vertinami dėl greitų ir teisingų sprendimų priėmimo, atsakingų
                        darbų atlikimo, nuoširdaus bendravimo ir lankstumo.</p>
                    <p>Mūsų vizija - būti viena iš patikimiausių, labiausiai vertinamų ir pirmaujančių agentūrų,
                        teikiančių kokybiškas nekilnojamojo turto paslaugas Šilutės raj.</p>
                    <p>Mūsų misija - siekti, kad kiekvienam klientui būtų suteiktos aukščiausio lygio paslaugos
                        susijusios su nekilnojamuoju turtu, operatyviu ir profesionaliu darbu pateisinti klientų
                        lūkesčius, siekti, jog mus su klientais sietų ilgalaikiai bendradarbiavimo ryšiai.</p>

                    <div class="about-bottom">
                        <a class="bottom-btn" href="#">Skaityti daugiau
                            <i class="fas fa-chevron-right"></i></a>
                    </div>
                    <!-- /.about-bottom -->
                </div>
            </div>

            <div class="col-lg-6 no-padding d-none d-lg-block">
                <div class="about-image">
                    <a href="#"><img class="img-fluid" src="{{ asset('images/about.png') }}" alt="NT Šilutė"></a>
                </div>
            </div>
        </div>

    </div>

</section>
<!-- /.sect-about section -->

<section class="sect-services">

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-6 no-padding d-none d-lg-block">
                <div class="service-image">
                    <a href="#"><img class="img-fluid" src="{{ asset('images/services.png') }}" alt="NT Šilutė"></a>
                </div>
            </div>

            <div class="col-lg-6 no-padding">
                <div class="info">
                    <h1 class="section-title">Paslaugos</h1>

                    <ul class="service-list">
                        <li class="service-list--item">
                            <a href="#">Tarpininkaujame perkant, parduodant bei nuomojant nekilnojamąjį turtą
                                <i class="fas fa-chevron-right"></i></a>
                        </li>
                        <li class="service-list--item">
                            <a href="#">Paruošiame visus nekilnojamojo turto pirkimui - pardavimui reikalingus
                                dokumentus
                                <i class="fas fa-chevron-right"></i></a>
                        </li>
                        <li class="service-list--item">
                            <a href="#">Koordinuojame ir tarpininkaujame pirkimo - pardavimo procese
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                        <li class="service-list--item">
                            <a href="#">Tarpininkaujame atliekant nekilnojamojo turto vertinimą
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                        <li class="service-list--item">
                            <a href="#">Užsakome butų, namų, pastatų energetinio naudingumo sertifikatus
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.services-list -->

                    <div class="service-bottom">
                        <a class="bottom-btn" href="#">Skaityti daugiau
                            <i class="fas fa-chevron-right"></i></a>
                    </div>
                    <!-- /.service-bottom -->
                </div>
            </div>
        </div>

    </div>

</section>
<!-- /.sect-about section -->

<section class="sect-contacts section">

    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="section-title">Konsultacija</h1>
                <p>Ieškote nekilnojamojo turto arba norite parduoti, susisiekite su mumis ir mes Jums padėsime.</p>
            </div>
        </div>

        <form class="contact-form">
            <div class="row">
                <div class="col col-sm-6 col-12">
                    <input type="text" class="form-control custom-input" placeholder="Vardas">
                    <input type="text" class="form-control custom-input" placeholder="Telefono numeris">
                    <input type="text" class="form-control custom-input" placeholder="El. paštas">
                </div>
                <div class="col col-sm-6 col-12">
                    <textarea class="form-control custom-textarea" placeholder="Jūsų užklausa" rows="3"></textarea>
                </div>
            </div>

            <div class="contact-bottom">
                <div class="bottom-btn" href="#">Siųsti užklausą
                    <i class="fas fa-chevron-right"></i></div>
            </div>
            <!-- /.contact-bottom -->

        </form>
        <!-- /.contact-form -->

    </div>

</section>
<!-- /.contacts section -->

@endsection