@extends('layouts.app')

@section('title', 'NT paslaugos Šilutėje')
@section('content')

@include('include.headerNoSlider')

<section class="page-about sect-content section-content">

    <div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="section-title">Apie mus</h1>

                <p>NT Šilutė - Nekilnojamojo turto agentūra Šilutėje įkurta 2008m. ir nuo to laiko sėkmingai dirbanti nekilnojamojo turto pirkimo, pardavimo bei nuomos srityse.&nbsp;Esame&nbsp;viena iš pirmųjų Šilutės miesto&nbsp;&nbsp;agentūrų, kuri savo pastoviu ir kvalifikuotu darbu, bei lanksčia mokesčių sistema pelnėme klientų pasitikėjimą. Mes vertinami dėl greitų ir teisingų sprendimų priėmimo, atsakingų darbų atlikimo, nuoširdaus bendravimo ir lankstumo.</p>
                <p>Mūsų vizija - būti viena iš patikimiausių, labiausiai vertinamų&nbsp;ir&nbsp;pirmaujančių agentūrų,&nbsp;teikiančių kokybiškas nekilnojamojo turto&nbsp;paslaugas Šilutės raj.</p>
                <p>Mūsų misija - siekti, kad kiekvienam klientui būtų suteiktos aukščiausio lygio paslaugos susijusios su nekilnojamuoju turtu, operatyviu ir profesionaliu darbu pateisinti klientų lūkesčius, siekti, jog mus su klientais sietų ilgalaikiai bendradarbiavimo ryšiai.</p>
                <p>Mūsų vertybės:<br>
                    - Išskirtinis dėmesys klientui ir jo&nbsp; poreikiams;<br>
                    - Lankstus požiūris ir atvirumas naujovėms;<br>
                    - Noras pirmauti ir tobulėti;</p>
            </div>
            <!-- /.col-lg-12 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

</section>
<!-- /.estates section -->

@endsection