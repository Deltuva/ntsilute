@extends('layouts.app')

@section('title', 'NT Šilutėje kontaktai - NT Šilutė')
@section('content')

@include('include.headerNoSlider')

<section class="page-contact sect-content section-content">

    <div class="container">

        <div class="row">

            <div class="col-lg-4 col-sm-6 col-md-6">
                <div id="map"></div>
            </div>
            <!-- /.col-lg-4 -->

            <div class="col-lg-4 col-sm-6 col-md-6">
                <h1>Kontaktai</h1>
                <p><strong>Adresas:</strong> Sodų g. 6-2 Šilutė</p>
                <p><strong>Telefonas:</strong> +370 611 20364</p>
                <p><strong>El. paštas:</strong> <a href="mailto:info@ntsilute.lt">info@ntsilute.lt</a></p>
            </div>
            <!-- /.col-lg-4 -->

            <div class="col-lg-4 col-sm-12 col-md-12">
                <h1>Konsultacija</h1>
                <p>Ieškote nekilnojamojo turto arba norite parduoti, susisiekite su mumis ir mes Jums padėsime.</p>

                <form class="contact-form">
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control custom-input" placeholder="Jūsų vardas">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col col-lg-12 col-12">
                            <input type="text" class="form-control custom-input" placeholder="Telefono numeris">
                        </div>
                        <div class="col col-lg-12 col-12">
                            <input type="text" class="form-control custom-input" placeholder="El. paštas">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <textarea class="form-control custom-textarea" placeholder="Žinutė" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="contact-bottom">
                        <button type="button" class="bottom-btn">Siųsti užklausą
                            <i class="fas fa-chevron-right"></i></button>
                    </div>
                    <!-- /.contact-bottom -->

                </form>
                <!-- /.contact-form -->
            </div>
            <!-- /.col-lg-4 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

</section>
<!-- /.estates section -->

@push('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_JS_API') }}&callback=initMap"></script>
<script type="text/javascript">
	function initMap() {
			var marker;
			var mapOptions = {
				zoom: 16,
				center: new google.maps.LatLng(56.0600622, 21.4835102),
				mapTypeId: google.maps.MapTypeId.TERRAIN,
			};

			map = new google.maps.Map(document.getElementById('map'), mapOptions);

			var marker = new google.maps.Marker({
				position: {
					lat: 56.0600622,
					lng: 21.4835102,
				},
				map: map
			});
		}
</script>
@endpush

@endsection