@extends('layouts.app')

@section('title', 'NT paslaugos Šilutėje')
@section('content')

@include('include.headerNoSlider')

<section class="page-service sect-services section-content">

    <div class="container-fluid">

        <div class="row">

            <div class="col-lg-6 no-padding d-none d-lg-block">

                <div class="service-image">
                    <a href="#"><img class="img-fluid" src="{{ asset('images/services.png') }}" alt="NT Šilutė"></a>
                </div>

            </div>
            <!-- /.col-lg-6 -->

            <div class="col-lg-6 no-padding">
                <div class="info">
                    <h1 class="section-title">Paslaugos</h1>

                    <ul class="service-list">
                        <li class="service-list--item">
                            <a href="#">Tarpininkaujame perkant, parduodant bei nuomojant nekilnojamąjį turtą
                                <i class="fas fa-chevron-right"></i></a>
                        </li>
                        <li class="service-list--item">
                            <a href="#">Paruošiame visus nekilnojamojo turto pirkimui - pardavimui reikalingus
                                dokumentus
                                <i class="fas fa-chevron-right"></i></a>
                        </li>
                        <li class="service-list--item">
                            <a href="#">Koordinuojame ir tarpininkaujame pirkimo - pardavimo procese
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                        <li class="service-list--item">
                            <a href="#">Tarpininkaujame atliekant nekilnojamojo turto vertinimą
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                        <li class="service-list--item">
                            <a href="#">Užsakome butų, namų, pastatų energetinio naudingumo sertifikatus
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                        <li class="service-list--item">
                            <a href="#">Tarpininkaujame perkant, parduodant bei nuomojant nekilnojamąjį turtą
                                <i class="fas fa-chevron-right"></i></a>
                        </li>
                        <li class="service-list--item">
                            <a href="#">Paruošiame visus nekilnojamojo turto pirkimui - pardavimui reikalingus
                                dokumentus
                                <i class="fas fa-chevron-right"></i></a>
                        </li>
                        <li class="service-list--item">
                            <a href="#">Koordinuojame ir tarpininkaujame pirkimo - pardavimo procese
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.services-list -->
                </div>
            </div>
            <!-- /.col-lg-6 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

</section>
<!-- /.estates section -->

@endsection