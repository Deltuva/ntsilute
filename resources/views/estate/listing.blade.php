@extends('layouts.app')

@section('title', 'Parduodamas NT Šilutėje')
@section('content')

@include('include.headerNoSlider')
@include('include.banner')

<section class="sect-content section-content fixed-padding">

    <div class="container">

        <div class="row">

            <div class="col-lg-3 col-md-4 pr-2">

                <div class="sidecategories">
                    <h1 class="sidetitle">NT skelbimai</h1>

                    <ul class="sidecategories-list">
                        <li class="sidecategories-list--item"><a class="icnb-butai on" href="#"><span>Butai</span> <i
                            class="fas fa-chevron-right"></i></a></li>
                        <li class="sidecategories-list--item"><a class="icnb-komercines-patalpos" href="#">Komercinės patalpos <i class="fas fa-chevron-right"></i></a></li>
                        <li class="sidecategories-list--item"><a class="icnb-namai" href="#">Namai <i
                            class="fas fa-chevron-right"></i></a></li>
                        <li class="sidecategories-list--item"><a class="icnb-sklypai" href="#">Sklypai <i
                            class="fas fa-chevron-right"></i></a></li>
                        <li class="sidecategories-list--item"><a class="icnb-sodybos" href="#">Sodybos <i
                            class="fas fa-chevron-right"></i></a></li>
                    </ul>
                    <!-- /.sidecategories-list -->

                </div>
                <!-- /.sidecategories -->

            </div>
            <!-- /.col-md-3 -->

            <div class="col-lg-9 col-md-8 order-first order-lg-2 order-md-2">

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="title">Parduodamas NT Šilutėje</h1>
                    </div>
                </div>

                <div class="estate">
                    <div class="row">

                        <div class="estate-block col-lg-4 col-sm-6 col-md-6 col-12 ml-auto mr-auto pr-2">
                            <div class="estate-item">
                                <div class="estate-item--img">
                                    <a href="{{ route('content.show') }}"><img class="img-fluid"
                                            src="{{ asset('images/estates/1.jpg') }}" alt="Image"></a>
                                </div>
                                <div class="estate-item--content">
                                    <a href="{{ route('content.show') }}">PARDUODAMAS 2 KAMBARIŲ BUTAS 2 AUKŠTE</a>

                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Adresas</span>
                                        <span class="estate-item--content-line--r">Žalgirio g. 7-10, Šilutė</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Buto plotas</span>
                                        <span class="estate-item--content-line--r">36.57</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Kambarių sk.</span>
                                        <span class="estate-item--content-line--r">2</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Aukštas</span>
                                        <span class="estate-item--content-line--r">2</span>
                                    </div>

                                </div>
                                <!-- /.estate-item--content -->

                                <div class="estate-item--price">
                                    <div class="price">26000&euro;</div>
                                    <div><a class="item-btn" href="{{ route('content.show') }}">Žiūrėti
                                            <i class="fas fa-chevron-right float-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <!-- /.estate-block -->

                        <div class="estate-block col-lg-4 col-sm-6 col-md-6 col-12 ml-auto mr-auto pr-2">
                            <div class="estate-item">
                                <div class="estate-item--img">
                                    <a href="{{ route('content.show') }}"><img class="img-fluid"
                                            src="{{ asset('images/estates/2.jpg') }}" alt="Image"></a>
                                    <span class="status-rezervuota"></span>
                                </div>
                                <div class="estate-item--content">
                                    <a href="{{ route('content.show') }}">PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE</a>

                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Adresas</span>
                                        <span class="estate-item--content-line--r">Žalgirio g. 7-10, Šilutė</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Buto plotas</span>
                                        <span class="estate-item--content-line--r">36.57</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Kambarių sk.</span>
                                        <span class="estate-item--content-line--r">2</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Aukštas</span>
                                        <span class="estate-item--content-line--r">3</span>
                                    </div>

                                </div>
                                <!-- /.estate-item--content -->

                                <div class="estate-item--price">
                                    <div class="price">36000&euro;</div>
                                    <div><a class="item-btn" href="{{ route('content.show') }}">Žiūrėti <i
                                                class="fas fa-chevron-right float-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <!-- /.estate-block -->

                        <div class="estate-block col-lg-4 col-sm-6 col-md-6 col-12 ml-auto mr-auto pr-2">
                            <div class="estate-item">
                                <div class="estate-item--img">
                                    <a href="{{ route('content.show') }}"><img class="img-fluid"
                                            src="{{ asset('images/estates/3.jpg') }}" alt="Image"></a>
                                    <span class="status-parduota"></span>
                                </div>
                                <div class="estate-item--content">
                                    <a href="{{ route('content.show') }}">PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE</a>

                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Adresas</span>
                                        <span class="estate-item--content-line--r">Žalgirio g. 7-10, Šilutė</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Buto plotas</span>
                                        <span class="estate-item--content-line--r">36.57</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Kambarių sk.</span>
                                        <span class="estate-item--content-line--r">2</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Aukštas</span>
                                        <span class="estate-item--content-line--r">3</span>
                                    </div>

                                </div>
                                <!-- /.estate-item--content -->

                                <div class="estate-item--price">
                                    <div class="price">6000&euro;</div>
                                    <div><a class="item-btn" href="{{ route('content.show') }}">Žiūrėti
                                            <i class="fas fa-chevron-right float-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <!-- /.estate-block -->

                        <div class="estate-block col-lg-4 col-sm-6 col-md-6 col-12 ml-auto mr-auto pr-2">
                            <div class="estate-item">
                                <div class="estate-item--img">
                                    <a href="{{ route('content.show') }}"><img class="img-fluid"
                                            src="{{ asset('images/estates/1.jpg') }}" alt="Image"></a>
                                </div>
                                <div class="estate-item--content">
                                    <a href="{{ route('content.show') }}">PARDUODAMAS 2 KAMBARIŲ BUTAS 2 AUKŠTE</a>

                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Adresas</span>
                                        <span class="estate-item--content-line--r">Žalgirio g. 7-10, Šilutė</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Buto plotas</span>
                                        <span class="estate-item--content-line--r">36.57</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Kambarių sk.</span>
                                        <span class="estate-item--content-line--r">2</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Aukštas</span>
                                        <span class="estate-item--content-line--r">2</span>
                                    </div>

                                </div>
                                <!-- /.estate-item--content -->

                                <div class="estate-item--price">
                                    <div class="price">26000&euro;</div>
                                    <div><a class="item-btn" href="{{ route('content.show') }}">Žiūrėti
                                            <i class="fas fa-chevron-right float-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <!-- /.estate-block -->

                        <div class="estate-block col-lg-4 col-sm-6 col-md-6 col-12 ml-auto mr-auto pr-2">
                            <div class="estate-item">
                                <div class="estate-item--img">
                                    <a href="{{ route('content.show') }}"><img class="img-fluid"
                                            src="{{ asset('images/estates/2.jpg') }}" alt="Image"></a>
                                </div>
                                <div class="estate-item--content">
                                    <a href="{{ route('content.show') }}">PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE</a>

                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Adresas</span>
                                        <span class="estate-item--content-line--r">Žalgirio g. 7-10, Šilutė</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Buto plotas</span>
                                        <span class="estate-item--content-line--r">36.57</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Kambarių sk.</span>
                                        <span class="estate-item--content-line--r">2</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Aukštas</span>
                                        <span class="estate-item--content-line--r">3</span>
                                    </div>

                                </div>
                                <!-- /.estate-item--content -->

                                <div class="estate-item--price">
                                    <div class="price">36000&euro;</div>
                                    <div><a class="item-btn" href="{{ route('content.show') }}">Žiūrėti <i
                                                class="fas fa-chevron-right float-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <!-- /.estate-block -->

                        <div class="estate-block col-lg-4 col-sm-6 col-md-6 col-12 ml-auto mr-auto pr-2">
                            <div class="estate-item">
                                <div class="estate-item--img">
                                    <a href="{{ route('content.show') }}"><img class="img-fluid"
                                            src="{{ asset('images/no-photo.png') }}" alt="Image"></a>
                                </div>
                                <div class="estate-item--content">
                                    <a href="{{ route('content.show') }}">PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE</a>

                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Adresas</span>
                                        <span class="estate-item--content-line--r">Žalgirio g. 7-10, Šilutė</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Buto plotas</span>
                                        <span class="estate-item--content-line--r">36.57</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Kambarių sk.</span>
                                        <span class="estate-item--content-line--r">2</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Aukštas</span>
                                        <span class="estate-item--content-line--r">3</span>
                                    </div>

                                </div>
                                <!-- /.estate-item--content -->

                                <div class="estate-item--price">
                                    <div class="price">6000&euro;</div>
                                    <div><a class="item-btn" href="{{ route('content.show') }}">Žiūrėti
                                            <i class="fas fa-chevron-right float-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <!-- /.estate-block -->

                        <div class="estate-block col-lg-4 col-sm-6 col-md-6 col-12 ml-auto mr-auto pr-2">
                            <div class="estate-item">
                                <div class="estate-item--img">
                                    <a href="{{ route('content.show') }}"><img class="img-fluid"
                                            src="{{ asset('images/estates/1.jpg') }}" alt="Image"></a>
                                </div>
                                <div class="estate-item--content">
                                    <a href="{{ route('content.show') }}">PARDUODAMAS 2 KAMBARIŲ BUTAS 2 AUKŠTE</a>

                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Adresas</span>
                                        <span class="estate-item--content-line--r">Žalgirio g. 7-10, Šilutė</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Buto plotas</span>
                                        <span class="estate-item--content-line--r">36.57</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Kambarių sk.</span>
                                        <span class="estate-item--content-line--r">2</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Aukštas</span>
                                        <span class="estate-item--content-line--r">2</span>
                                    </div>

                                </div>
                                <!-- /.estate-item--content -->

                                <div class="estate-item--price">
                                    <div class="price">26000&euro;</div>
                                    <div><a class="item-btn" href="{{ route('content.show') }}">Žiūrėti
                                            <i class="fas fa-chevron-right float-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <!-- /.estate-block -->

                        <div class="estate-block col-lg-4 col-sm-6 col-md-6 col-12 ml-auto mr-auto pr-2">
                            <div class="estate-item">
                                <div class="estate-item--img">
                                    <a href="{{ route('content.show') }}"><img class="img-fluid"
                                            src="{{ asset('images/estates/2.jpg') }}" alt="Image"></a>
                                </div>
                                <div class="estate-item--content">
                                    <a href="{{ route('content.show') }}">PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE</a>

                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Adresas</span>
                                        <span class="estate-item--content-line--r">Žalgirio g. 7-10, Šilutė</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Buto plotas</span>
                                        <span class="estate-item--content-line--r">36.57</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Kambarių sk.</span>
                                        <span class="estate-item--content-line--r">2</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Aukštas</span>
                                        <span class="estate-item--content-line--r">3</span>
                                    </div>

                                </div>
                                <!-- /.estate-item--content -->

                                <div class="estate-item--price">
                                    <div class="price">36000&euro;</div>
                                    <div><a class="item-btn" href="{{ route('content.show') }}">Žiūrėti <i
                                                class="fas fa-chevron-right float-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <!-- /.estate-block -->

                        <div class="estate-block col-lg-4 col-sm-6 col-md-6 col-12 ml-auto mr-auto pr-2">
                            <div class="estate-item">
                                <div class="estate-item--img">
                                    <a href="{{ route('content.show') }}"><img class="img-fluid"
                                            src="{{ asset('images/estates/3.jpg') }}" alt="Image"></a>
                                </div>
                                <div class="estate-item--content">
                                    <a href="{{ route('content.show') }}">PARDUODAMAS 2 KAMBARIŲ BUTAS 3 AUKŠTE</a>

                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Adresas</span>
                                        <span class="estate-item--content-line--r">Žalgirio g. 7-10, Šilutė</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Buto plotas</span>
                                        <span class="estate-item--content-line--r">36.57</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Kambarių sk.</span>
                                        <span class="estate-item--content-line--r">2</span>
                                    </div>
                                    <div class="estate-item--content-line">
                                        <span class="estate-item--content-line--l">Aukštas</span>
                                        <span class="estate-item--content-line--r">3</span>
                                    </div>

                                </div>
                                <!-- /.estate-item--content -->

                                <div class="estate-item--price">
                                    <div class="price">6000&euro;</div>
                                    <div><a class="item-btn" href="{{ route('content.show') }}">Žiūrėti
                                            <i class="fas fa-chevron-right float-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <!-- /.estate-block -->

                    </div>

                    {{-- <div class="estate-bottom">
                        <a class="bottom-btn" href="#">Žiūrėti daugiau
                            <i class="fas fa-chevron-right"></i></a>
                    </div> --}}
                    <!-- /.estate-bottom -->

                </div>
                <!-- /.estate -->

            </div>
            <!-- /.col-md-9 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

</section>
<!-- /.estates section -->

@endsection