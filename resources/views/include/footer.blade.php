<footer class="footer">
    <div class="container">

        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="footer__copyright">
                    Copyright &copy; 2018 Šilutės nekilnojamojo turto agentūra. Visos teisės saugomos.
                </div>
                <!-- /.footer__copyright -->
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="footer__developer">
                    Sprendimas: <a href="#" title="Deltuva">Deltuva</a>
                </div>
                <!-- /.footer__developer -->
            </div>

        </div>
    </div>

</footer>
<!-- /.footer -->