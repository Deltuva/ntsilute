<div class="modal" id="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Užklausa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- /.modal-header -->

            <div class="modal-body">
                <form class="modal-form">
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control custom-input" placeholder="Jūsų vardas">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col col-lg-6 col-12">
                            <input type="text" class="form-control custom-input" placeholder="Telefono numeris">
                        </div>
                        <div class="col col-lg-6 col-12">
                            <input type="text" class="form-control custom-input" placeholder="El. paštas">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <textarea class="form-control custom-textarea" placeholder="Žinutė" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="modal-bottom">
                        <button type="button" class="bottom-btn">Siųsti
                            <i class="fas fa-chevron-right"></i></button>
                    </div>
                    <!-- /.contact-bottom -->

                </form>
                <!-- /.contact-form -->
            </div>
            <!-- /.modal-body -->
        </div>
        <!-- /.modal-content -->

    </div>
    <!-- /.modal-dialog -->

</div>
<!-- /.modal -->