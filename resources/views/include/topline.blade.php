<div class="top-line">

    <nav class="navbar navbar-expand-lg navbar-light custom-nav">

        <div class="container">

            <a class="navbar-brand" href="{{ route('page.home') }}" title="Butai, namai, sodai, sklypai Šilutėje">
                <img src="{{ asset('images/ntsilute-logo.png') }}" alt="NT Šilutė">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item active"><a class="nav-link" href="{{ route('page.about') }}">Apie mus</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('page.service') }}">Paslaugos</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('estate.listing') }}">NT skelbimai</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('page.contact') }}">Kontaktai</a></li>
                </ul>
            </div>

        </div>

    </nav>

</div>
<!-- /.top-line -->