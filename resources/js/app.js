/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

require("./libs/fontawesome_v5");
require("./main");

/**
 * Lightbox2
 */

lightbox.option({
  'resizeDuration': 200,
  'wrapAround': true,
  'albumLabel': "Nuotrauka %1 iš %2"
});