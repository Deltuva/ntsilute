<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\EstateRequest as StoreRequest;
use App\Http\Requests\EstateRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class EstateCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class EstateCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Estate');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/estate');
        $this->crud->setEntityNameStrings('estate', 'estates');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();

        // Name
        $this->crud->addColumn([
            'name'  => 'name',
            'label' => 'Pavadinimas',
            'type'  => 'text'
        ]);
        $this->crud->addField([
            'name'  => 'name',
            'label' => 'Pavadinimas:',
            'type'  => 'text'
        ]);

        // Category
        $this->crud->addColumn([
            'label'     => "Kategorija",
            'type'      => "select",
            'name'      => 'category_id',
            'entity'    => 'category',
            'attribute' => "name",
            'model'     => "App\Models\Category",
        ]);
        $this->crud->addField([
            'label'     => "Pasirinkite kategoriją:",
            'type'      => 'select',
            'name'      => 'category_id',
            'entity'    => 'category',
            'attribute' => 'name',
            'model'     => "App\Models\Category",
            'options'   => (function ($query) {
                    return $query->orderBy('name', 'ASC')->get();
                }),
            ]);

        // Address
        $this->crud->addColumn([
            'name'  => 'address',
            'label' => 'Adresas',
            'type'  => 'text'
        ]);
        $this->crud->addField([
            'name'  => 'address',
            'label' => 'Adresas:',
            'type'  => 'text'
        ]);

        // Status
        $this->crud->addColumn([
            'name'        => 'status',
            'label'       => 'Statusas',
            'type'        => 'select_from_array',
            'options'     => ['0' => 'Laisvas', '1' => 'Parduotas', '2' => "Rezervuotas"],
            'allows_null' => false,
            'default'     => '0'
        ]);
        $this->crud->addField([
            'name'        => 'status',
            'label'       => "Statusas:",
            'type'        => 'select_from_array',
            'options'     => ['0' => 'Laisvas', '1' => 'Parduotas', '2' => "Rezervuotas"],
            'allows_null' => false,
            'default'     => '0'
        ]);

        // Rooms
        $this->crud->addColumn([
            'name'  => 'number_of_apartment',
            'label' => 'Kambarių sk.',
            'type'  => 'text'
        ]);
        $this->crud->addField([
            'name'  => 'number_of_apartment',
            'label' => 'Kambarių sk.:',
            'type'  => 'text'
        ]);

        // Height
        $this->crud->addColumn([
            'name'  => 'floor_of_apartment',
            'label' => 'Aukštas',
            'type'  => 'text'
        ]);
        $this->crud->addField([
            'name'  => 'floor_of_apartment',
            'label' => 'Aukštas:',
            'type'  => 'text'
        ]);

        // Window of apartment
        $this->crud->addColumn([
            'name'        => 'window_of_apartment',
            'label'       => 'Vitrininiai langai',
            'type'        => 'select_from_array',
            'options'     => ['1' => 'Yra', '2' => "Nėra"],
            'allows_null' => true,
            'default'     => null
        ]);
        $this->crud->addField([
            'name'        => 'window_of_apartment',
            'label'       => "Vitrininiai langai:",
            'type'        => 'select_from_array',
            'options'     => ['1' => 'Yra', '2' => "Nėra"],
            'allows_null' => true,
            'default'     => null
        ]);

        // Heating
        $this->crud->addColumn([
            'name'        => 'heating',
            'label'       => 'Šildymas',
            'type'        => 'select_from_array',
            'options'     => ['1' => 'Yra', '2' => "Nėra"],
            'allows_null' => true,
            'default'     => null
        ]);
        $this->crud->addField([
            'name'        => 'heating',
            'label'       => "Šildymas:",
            'type'        => 'select_from_array',
            'options'     => ['1' => 'Yra', '2' => "Nėra"],
            'allows_null' => true,
            'default'     => null
        ]);

        // Electric
        $this->crud->addColumn([
            'name'        => 'electra',
            'label'       => 'Elektra',
            'type'        => 'select_from_array',
            'options'     => ['1' => 'Yra', '2' => "Nėra"],
            'allows_null' => true,
            'default'     => null
        ]);
        $this->crud->addField([
            'name'        => 'electra',
            'label'       => "Elektra:",
            'type'        => 'select_from_array',
            'options'     => ['1' => 'Yra', '2' => "Nėra"],
            'allows_null' => true,
            'default'     => null
        ]);

        // Water
        $this->crud->addColumn([
            'name'        => 'water_supply',
            'label'       => 'Vandentiekis',
            'type'        => 'select_from_array',
            'options'     => ['1' => 'Yra', '2' => "Nėra"],
            'allows_null' => true,
            'default'     => null
        ]);
        $this->crud->addField([
            'name'        => 'water_supply',
            'label'       => "Vandentiekis:",
            'type'        => 'select_from_array',
            'options'     => ['1' => 'Yra', '2' => "Nėra"],
            'allows_null' => true,
            'default'     => null
        ]);

        // Area weight
        $this->crud->addColumn([
            'name'  => 'area_of_apartment',
            'label' => 'Būto plotas',
            'type'  => 'text'
        ]);
        $this->crud->addField([
            'name'  => 'area_of_apartment',
            'label' => 'Būto plotas:',
            'type'  => 'text'
        ]);

        // Plot of area
        $this->crud->addColumn([
            'name'  => 'plotarea_of_house',
            'label' => 'Sklypo plotas',
            'type'  => 'text'
        ]);
        $this->crud->addField([
            'name'  => 'plotarea_of_house',
            'label' => 'Sklypo plotas:',
            'type'  => 'text'
        ]);

        // Area premise
        $this->crud->addColumn([
            'name'  => 'area_of_premise',
            'label' => 'Patalpų plotas',
            'type'  => 'text'
        ]);
        $this->crud->addField([
            'name'  => 'area_of_premise',
            'label' => 'Patalpų plotas:',
            'type'  => 'text'
        ]);

        // Area of house
        $this->crud->addColumn([
            'name'  => 'area_of_house',
            'label' => 'Namo plotas',
            'type'  => 'text'
        ]);
        $this->crud->addField([
            'name'  => 'area_of_house',
            'label' => 'Namo plotas:',
            'type'  => 'text'
        ]);

        // // Image
        // $this->crud->addColumn([
        //     'name'   => 'images',
        //     'label'  => 'Nuotrauka',
        //     'type'   => 'image',
        //     'prefix' => 'uploads/',
        // ]);
        // $this->crud->addField([
        //     'name'   => 'images',
        //     'label'  => 'Pasirinkite nuotraukas įkėlimui:',
        //     'type'   => 'upload_multiple',
        //     'upload' => true,
        //     'disk'   => 'uploads'
        // ]);

        // Crypt
        $this->crud->addColumn([
            'name'        => 'cellar',
            'label'       => 'Rūsys',
            'type'        => 'select_from_array',
            'options'     => ['1' => 'Yra', '2' => "Nėra"],
            'allows_null' => true,
            'default'     => null
        ]);
        $this->crud->addField([
            'name'        => 'cellar',
            'label'       => "Rūsys:",
            'type'        => 'select_from_array',
            'options'     => ['1' => 'Yra', '2' => "Nėra"],
            'allows_null' => true,
            'default'     => null
        ]);

        // Year
        $this->crud->addColumn([
            'name'  => 'year',
            'label' => 'Statybos metai',
            'type'  => 'text'
        ]);
        $this->crud->addField([
            'name'  => 'year',
            'label' => 'Statybos metai:',
            'type'  => 'text'
        ]);

        $this->crud->addColumn([
            'name'  => 'body',
            'label' => 'Daugiau'
        ]);
        $this->crud->addField([
            'name'  => 'body',
            'label' => 'Daugiau:',
            'type'  => 'ckeditor',
            // optional:
            'extra_plugins' => ['oembed', 'widget'],
            'options'       => [
            'autoGrow_minHeight'   => 200,
            'autoGrow_bottomSpace' => 50,
            'removePlugins'        => 'resize,maximize',
            ]
        ]);

        // Slug
        $this->crud->addColumn([
            'name'  => 'slug',
            'label' => 'Nuoroda',
            'type'  => 'text'
        ]);
        $this->crud->addField([
            'name'  => 'slug',
            'label' => 'Nuoroda:',
            'type'  => 'text'
        ]);

        // Price
        $this->crud->addColumn([
            'name'  => 'price',
            'label' => 'Kaina',
            'type'  => 'number'
        ]);
        $this->crud->addField([
            'name'   => 'price',
            'label'  => 'Kaina',
            'type'   => 'number',
            'prefix' => "&euro;"
        ]);

        // add asterisk for fields that are required in EstateRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
